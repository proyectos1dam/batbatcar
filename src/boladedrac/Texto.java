package boladedrac;

public class Texto {
    
    final static String RESET = "\u001B[0m";
    final static String ROJO = "\033[31m";
    final static String VERDE = "\033[32m";
    final static String AMARILLO = "\033[33m";
    final static String AZUL = "\033[34m";
    final static String MORADO = "\033[35m";
    final static String CYAN = "\033[36m";
    final static String FONDO_NEGRO = "\u001B[40m";
    final static String FONDO_ROJO = "\u001B[41m";
    final static String FONDO_VERDE = "\u001B[42m";
    final static String FONDO_AMARILLO = "\u001B[43m";
    final static String FONDO_AZUL = "\u001B[44m";
    final static String FONDO_MORADO = "\u001B[45m";
    final static String FONDO_CYAN = "\u001B[46m";
    final static String FONDO_BLANCO = "\u001B[47m";
    
    final static String BOLA = FONDO_ROJO;
    final static String FNIVEL = FONDO_AMARILLO;
    
    public static void textoInicio() {
        System.out.println("BIENVENIDOS A LA AVENTURA DE DRAGON BALL");
        System.out.println("========================================");
        System.out.println("\n" + ROJO +"Son Goku"+ RESET +" se encuentra tranquilamente en casa junto a su hijo " + CYAN +"Son Gohan"+ RESET +", a quien le está ayudando\na resolver unos deberes que le han mandado hacer en la escuela, concretamente son de matemáticas.");
        System.out.println("\nDe repente, aparece en casa " + AMARILLO +"el maestro tortuga"+ RESET +" y les cuenta que el malvado villano" + MORADO +" Freezer"+ RESET +" está\nplaneando encontrar las 7 bolas del dragón para pedir un deseo y obtener un poder que nadie\npodrá nunca parar. El planeta está en peligro.");
        Dibujos.tortuga();
        System.out.println("\n" + AMARILLO +"El maestro tortuga"+ RESET +" te pide ayuda. ¿Quieres que " + ROJO +"Son Goku"+ RESET +" y " + CYAN +"Son Gohan"+ RESET +" acepten el reto?");
        System.out.println("\nIntroduce el número [1] para aceptar el reto");
    }
    
    public static void textoNivel1(int n1, int n2) {
        Funciones.limpiarPantalla();
        System.out.println(FNIVEL+ ROJO+ " _   _ _____     _______ _        _ \n" +FNIVEL+ ROJO+
"| \\ | |_ _\\ \\   / / ____| |      / |\n" +FNIVEL+ ROJO+
"|  \\| || | \\ \\ / /|  _| | |      | |\n" +FNIVEL+ ROJO+
"| |\\  || |  \\ V / | |___| |___   | |\n" +FNIVEL+ ROJO+
"|_| \\_|___|  \\_/  |_____|_____|  |_|" + RESET);
        System.out.println("\n" + ROJO +"Son Goku"+ RESET +": Muy bien! Vamos a buscar las bolas de dragón antes de que " + MORADO +"Freezer"+ RESET +" pueda tenerlas.\n--dijo " + ROJO +"Son Goku"+ RESET +" desde su casa en el número " + ROJO + n1 + RESET + " de Ciudad del Norte.");
        System.out.println("\n" + ROJO +"Goku"+ RESET +" recordó que la última vez que las bolas fueron repartidas por el mundo de manera aleatoria,\nuna de ellas fue a parar en la zona donde actualmente vive su amigo " + VERDE +"Piccolo"+ RESET +" y que probablemente él la tenga en su poder.");
        System.out.println("\nMarcharon pues hacia el número " + ROJO + n2 + RESET +" de la Ciudad Maravilla, que es donde él vive. Después de un largo camino,\nhemos llegado a la casa de " + VERDE +"Piccolo"+ RESET +" y, en efecto, él tiene una de las bolas. Pero no quiere dárnosla gratuitamente,\npuesto que le tiene mucho aprecio. Nos propone la siguiente lucha matemática:");
        Dibujos.piccolo();
        System.out.println("\n" + VERDE +"Piccolo"+ RESET +": ¿Sabríais decirme cuál es el sumatorio entre el número de vuestra casa y el número de la mía?\nSi lo adivináis, os daré la bola y me uniré a vuestro equipo.\n");
    }
    
    public static void textoNivel2(int b1, int b2, String cadena1, String cadena2) {
        Funciones.limpiarPantalla();
        System.out.println(FNIVEL + ROJO + " _   _ _____     _______ _          ____  \n" +FNIVEL + ROJO +
"| \\ | |_ _\\ \\   / / ____| |        |___ \\ \n" +FNIVEL + ROJO +
"|  \\| || | \\ \\ / /|  _| | |          __) |\n" +FNIVEL + ROJO +
"| |\\  || |  \\ V / | |___| |___      / __/ \n" +FNIVEL + ROJO +
"|_| \\_|___|  \\_/  |_____|_____|    |_____|" + RESET);
        System.out.println("\n" + VERDE +"Piccolo"+ RESET +": Sois unos cracks de las matemáticas! Pensaba que solo sabíais luchar. Aquí tenéis la bola de " + b1 + " estrellas. ");
        System.out.println(BOLA + "( " + b1 + " )"+ RESET);
        System.out.println("BOLAS CONSEGUIDAS: " + BOLA + "( " + b1 + " )"+ RESET);
        System.out.println("\nPor cierto, tengo el número de teléfono de " + AMARILLO +"Ten Shin Han"+ RESET +", quién me envió un WhatsApp la semana pasada para contarme\nque había encontrado la bola de " + b2 + " estrellas en el Desierto del Olvido. Vamos!");
        System.out.println("\nSolo llegar, encontraron a " + AMARILLO +"Ten Shin Han"+ RESET +", el cual no tenía su mejor día y no les recibió con buena cara.\nEstaba muy cabreado porque estaba intentando resolver un enigma matemático y no veía forma de hacerlo.");
        System.out.println("\n" + CYAN +"Son Gohan"+ RESET +": Hola " + AMARILLO +"Ten Shin Han"+ RESET +", necesitamos que nos des tu bola de dragón.");
        Dibujos.tenShinHan();
        System.out.println("\n" + AMARILLO +"Ten Shin Han"+ RESET +": Sí, un momento, resulta que llevo así como 3 horas para descifrar un enigma alfabético llamado\n“La mezcla sagrada de cadenas de texto” y no veo la solución. Si me ayudáis a resolverlo, os daré la bola.\n\nTienes que mezclar las dos cadenas sagradas: " + ROJO + cadena1 + RESET + " y " + ROJO + cadena2 + RESET);
    }
    
    public static void textoNivel3(int b1, int b2, int b3) {
        Funciones.limpiarPantalla();
        System.out.println(FNIVEL + ROJO + " _   _ _____     _______ _          _____ \n" + FNIVEL + ROJO +
"| \\ | |_ _\\ \\   / / ____| |        |___ / \n" +FNIVEL + ROJO +
"|  \\| || | \\ \\ / /|  _| | |          |_ \\ \n" +FNIVEL + ROJO +
"| |\\  || |  \\ V / | |___| |___      ___) |\n" +FNIVEL + ROJO +
"|_| \\_|___|  \\_/  |_____|_____|    |____/ " + RESET);
        System.out.println("\n" + AMARILLO +"Ten Shin Han"+ RESET +": Wow! Gracias, aquí tenéis la bola de " + b2 + " estrellas.");
        System.out.println(BOLA + "( " + b2 + " )"+ RESET);
        System.out.println("BOLAS CONSEGUIDAS: " + BOLA + "( " + b1 + " )"+ RESET + " " + BOLA + "( " + b2 + " )"+ RESET);
        System.out.println("\nPor cierto, supe que nuestro enemigo " + AZUL +"Boo"+ RESET +" encontró una bola de dragón en su mudanza en “Montaña Perdida”.\nOs guiaré a la cueva de la montaña donde vive.\nNo es lejos de aquí.");
        System.out.println("\n" + AZUL +"Boo"+ RESET +" se encontraba descansando a la cueva.\nPuesto que tuvo muchos problemas en sus anteriores enfrentamientos con " + ROJO +"Goku"+ RESET +", este no tenía intención de luchar más con él,\npero al verlo qué iba a por su estimada bola de dragón de " + b3 + " estrellas, no reaccionó de buenas maneras.");
        Dibujos.boo();
        System.out.println("\n" + AZUL +"Boo"+ RESET +": Qué queréis? Mi bola de dragón?");
        System.out.println("\n" + ROJO +"Son Goku"+ RESET +": Necesitamos reunirlas todas antes de que " + MORADO +"Freezer"+ RESET +".\nEl mundo corre un gran peligro! Sabemos que tú ya no quieres hacer daño al planeta.\nTe lo pedimos por favor, por el bien de todos.");
        System.out.println("\n" + AZUL +"Boo"+ RESET +": Pero es mía! La encontré yo!\nAunque pensándolo mejor, solo os la daré si me decís la respuesta correcta a la solución de cuál es el factorial de " + b3 + ".\n");
    }
    
    public static void textoNivel4(int N, int b1, int b2, int b3) {
        Funciones.limpiarPantalla();
        System.out.println(FNIVEL + ROJO + " _   _ _____     _______ _          _  _   \n" + FNIVEL + ROJO +
"| \\ | |_ _\\ \\   / / ____| |        | || |  \n" +FNIVEL + ROJO +
"|  \\| || | \\ \\ / /|  _| | |        | || |_ \n" +FNIVEL + ROJO +
"| |\\  || |  \\ V / | |___| |___     |__   _|\n" +FNIVEL + ROJO +
"|_| \\_|___|  \\_/  |_____|_____|       |_|  " + RESET);
        System.out.println("\n" + AZUL +"Boo"+ RESET +": Vaya, os lo he puesto realmente fácil. Aquí tenéis la bola.");
        System.out.println(BOLA + "( " + b3 + " )"+ RESET);
        System.out.println("BOLAS CONSEGUIDAS: " + BOLA + "( " + b1 + " )"+ RESET + " " + BOLA + "( " + b2 + " )"+ RESET + " " + BOLA + "( " + b3 + " )"+ RESET);
        System.out.println("\nAhora bien, os avanzo que todavía tenéis un hueso duro de mordisquear.\n"+ VERDE + "Célula"+ RESET +" también las buscaba y escuché en la tele que ya tenía unas cuántas recogidas.\nSi no recuerdo mal, lo escuché en las noticias del canal número " + N + " de mi televisión.");
        System.out.println("\nSintonizaron el canal " + N + " y confirmaron que "+ VERDE + "Célula"+ RESET +" se encontraba en “Ciudad Nueva”.\nRápidamente fueron a enfrentarse con él y así conseguir las bolas que tenía en su poder.");
        Dibujos.celula();
        System.out.println("\n" + CYAN +"Son Gohan"+ RESET +": Hola "+ VERDE + "Célula"+ RESET +", no queremos problemas contigo.\nPor favor, danos tus bolas de dragón y no te haremos daño.");
        System.out.println("\n"+ VERDE + "Célula"+ RESET +": Jajaja. He entrenado desde la última lucha y ahora no podréis conmigo y, por supuesto,\nno podréis quitarme las bolas del dragón que tengo guardadas en esta caja.\nJuguemos a un juego, la caja tiene una contraseña, pero la única manera de conseguirla es descifrando esta adivinanza, jugamos?\n");
    }
    
    public static void textoNivel5(int b1, int b2, int b3, int b4, int b5, int b6, int b7) {
        Funciones.limpiarPantalla();
        System.out.println(FNIVEL + ROJO + " _   _ _____     _______ _          ____  \n" +FNIVEL + ROJO +
"| \\ | |_ _\\ \\   / / ____| |        | ___| \n" +FNIVEL + ROJO +
"|  \\| || | \\ \\ / /|  _| | |        |___ \\ \n" +FNIVEL + ROJO +
"| |\\  || |  \\ V / | |___| |___      ___) |\n" +FNIVEL + ROJO +
"|_| \\_|___|  \\_/  |_____|_____|    |____/ " + RESET);
        System.out.println("\nBOLAS CONSEGUIDAS: " + BOLA + "( " + b1 + " )"+ RESET + " " + BOLA + "( " + b2 + " )"+ RESET + " " + BOLA + "( " + b3 + " )"+ RESET);
        System.out.println("\n"+ VERDE + "Célula"+ RESET +": Malditos seáis! Lo habéis adivinado, pero....\nTengo que confesaros que iba de farol, y que no tengo ninguna bola de dragón.\nLo que sí puedo deciros es que el resto de bolas que necesitáis las tiene " + MORADO + "Freezer"+ RESET +".\nId a molestarlo a él.\nAdiós!");
        System.out.println("\n" + ROJO +"Son Goku"+ RESET +": Bien, vamos corriendo al planeta de " + MORADO + "Freezer"+ RESET +" a quitarle las bolas que restan.\nEsperamos que salga todo bien.\nVolaron en el Planeta Infernal donde " + MORADO + "Freezer"+ RESET +" esperaba a sus contrincantes para la lucha final.");
        System.out.println("\n" + ROJO +"Son Goku"+ RESET +": Hola " + MORADO + "Freezer"+ RESET +", sabemos que tramas. No lo permitiremos. Danos las bolas de dragón " + b4 + ", " + b5 + ", " + b6 + " y " + b7 + ".\nSabemos que las tienes.");
        Dibujos.freezzer();
        System.out.println("\n" + MORADO + "Freezer"+ RESET +": De ninguna forma! Dadme vosotros las vuestras.\nPero mirad, como no quiero luchar porque ya tengo una edad, os doy la posibilidad de resolver este conflicto con una lucha matemática.\nSi sabeis decirme cuál es el mínimo común múltiple de " + b4 + ", " + b5 + ", " + b6 + " y " + b7 +" todo habrá acabado.\n");
    }
    
    public static void textoHasGanado() {
        System.out.println("\nEnhorabuena!! Habéis conseguido las 7 bolas de Dragón.");
        System.out.println(BOLA + "( 1 )" + RESET + " " + BOLA + "( 2 )" + RESET + " " +BOLA + "( 3 )" + RESET + " " +BOLA + "( 4 )" + RESET + " " +BOLA + "( 5 )" + RESET + " " +BOLA + "( 6 )" + RESET + " " +BOLA + "( 7 )" + RESET);
        System.out.println("El mundo vuelve a respirar tranquilo.\n¡Hasta otra amigos!\n");
        Dibujos.hasGanado();
    }
    
    public static void textoHasPerdido() {
        System.out.println("\nDesgraciadamente, la aventura ha acabado y el mundo vuelve a ser un lugar inseguro.\n¡Una lástima!\n");
       Dibujos.hasPerdido();
    }
    
    public static void textoFin() {
        System.out.println("\n¡Adiós!");
    }
}