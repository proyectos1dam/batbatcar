package boladedrac;

public class Juego {
    
    public static void jugarPartida() {
        inicioPartida();
    }

    public static void inicioPartida() {
        Dibujos.portada();
        Funciones.pulsaCualquierTecla();
        Texto.textoInicio();
        int opcionElegida = Funciones.leerNumero("Introduce una opción: ");
        
        if (opcionElegida == 1) {
            nivel1();
        }
        else {
            fin();
        }
    }
    
    public static void nivel1() {
        int n1 = Funciones.generarNumeroAleatorio(0, 10);
        int n2 = Funciones.generarNumeroAleatorio(20, 30);
        int sumatorio = Funciones.calcularSumatorio(n1, n2);
        
        Texto.textoNivel1(n1, n2);
        System.out.print("¿Quieres una pista? [s/n]: ");
        String respuesta = Funciones.leerCadena("");
        if (respuesta.toLowerCase().equals("s")) {
            Funciones.ofrecerPista(sumatorio);
        }
        
        if (Funciones.comprobarRespuesta(sumatorio)) {
            nivel2();
        }
        else {
            hasPerdido();
        }
    }
    
    public static void nivel2() {
        int b1 = Funciones.generarNumeroAleatorio(1, 3);
        int b2 = Funciones.generarNumeroAleatorio(4, 7);
        String cadena1 = Funciones.generarCadenaAleatoria(b2);
        String cadena2 = Funciones.generarCadenaAleatoria(b2);
        String mezcla = Funciones.mezclaCadenas(cadena1, cadena2);
        
        Texto.textoNivel2(b1, b2, cadena1, cadena2);
        System.out.print("¿Quieres una pista? [s/n]: ");
        String respuesta = Funciones.leerCadena("");
        if (respuesta.toLowerCase().equals("s")) {
            Funciones.ofrecerPista(mezcla);
        }
        
        if (Funciones.comprobarRespuesta(mezcla)) {
            nivel3(b1, b2);
        }
        else {
            hasPerdido();
        }
    }
    
    public static void nivel3(int b1, int b2) {
        int b3;
        do {
            b3 = Funciones.generarNumeroAleatorio(4, 7);
        } while (b3 == b2);
        
        int factorial = Funciones.calcularFactorial(b3);
        
        Texto.textoNivel3(b1, b2, b3);
        System.out.print("¿Quieres una pista? [s/n]: ");
        String respuesta = Funciones.leerCadena("");
        if (respuesta.toLowerCase().equals("s")) {
            Funciones.ofrecerPista(factorial);
        }
        
        if (Funciones.comprobarRespuesta(factorial)) {
            nivel4(b1, b2, b3);
        }
        else {
            hasPerdido();
        }
    }
    
    public static void nivel4(int b1, int b2, int b3) {
        int N = Funciones.generarNumeroAleatorio(8, 12);
        String cadena1 = Funciones.generarCadenaVocalesAleatoria(N);
        String cadena2 = Funciones.generarCadenaVocalesAleatoria(N);
        String cadena3 = Funciones.generarCadenaVocalesAleatoria(N);
        String cadVocales = Funciones.generarCadenaVocalesAleatoria(2);
        
        int contCoincidencias = Funciones.compararCadenaVocales(cadena1, cadVocales);
        contCoincidencias += Funciones.compararCadenaVocales(cadena2, cadVocales);
        contCoincidencias += Funciones.compararCadenaVocales(cadena3, cadVocales);
        
        int contCoincidInversa = Funciones.compararCadenaVocalesInversa(cadena1, cadVocales);
        contCoincidInversa += Funciones.compararCadenaVocalesInversa(cadena2, cadVocales);
        contCoincidInversa += Funciones.compararCadenaVocalesInversa(cadena3, cadVocales);
        
        Texto.textoNivel4(N, b1, b2, b3);
        System.out.printf("Las palabras son '%s', '%s' y '%s'. Las vocales son '%s'\n", cadena1, cadena2, cadena3, cadVocales);
        System.out.println("¡Dime cuántas coincidencias (y también inversas) hay de esas vocales en esas tres palabras!");
        System.out.print("¿Quieres una pista? [s/n]: ");
        String respuesta = Funciones.leerCadena("");
        if (respuesta.toLowerCase().equals("s")) {
            String pista = cadena1 + ", " + cadena2 + " y " + cadena3 + " tienen " + contCoincidencias + " coincidencias y " + contCoincidInversa + " coincidencias inversas"; 
            Funciones.ofrecerPista(pista);
        }
        
        System.out.println("Número de coincidencias: ");
        if (Funciones.comprobarRespuesta(contCoincidencias)) {
            System.out.println("Número de coincidencias inversas: ");
            if (Funciones.comprobarRespuesta(contCoincidInversa)) {
                nivel5(b1, b2, b3);
            }
        }
        else {
            hasPerdido();
        }
    }
    
    public static void nivel5(int b1, int b2, int b3) {
        int b4, b5, b6, b7;
        
        do {
            b4 = Funciones.generarNumeroAleatorio(4, 7);
        } while (b4 == b2 || b4 == b3);
        
        do {
            b5 = Funciones.generarNumeroAleatorio(4, 7);
        } while (b5 == b2 || b5 == b3 || b5 == b4);
        
        do {
            b6 = Funciones.generarNumeroAleatorio(1, 3);
        } while (b6 == b1);
        
        do {
            b7 = Funciones.generarNumeroAleatorio(1, 3);
        } while (b7 == b1 || b7 == b6);

        Texto.textoNivel5(b1, b2, b3, b4, b5, b6, b7);
        int mcm = Funciones.calcularMinimoComunMultiple(b4, b5, b6, b7);
        
        System.out.print("¿Quieres una pista? [s/n]: ");
        String respuesta = Funciones.leerCadena("");
        if (respuesta.toLowerCase().equals("s")) {
            Funciones.ofrecerPista(mcm);
        }
        if (Funciones.comprobarRespuesta(mcm)) {
            hasGanado();
        } else {
            hasPerdido();
        }
    }
    
    public static void hasGanado() {
        Texto.textoHasGanado();
        fin();
    }
    
    public static void hasPerdido() {
        Texto.textoHasPerdido();
        fin();
    }
    
    public static void fin() {
        Texto.textoFin();
    }
}
