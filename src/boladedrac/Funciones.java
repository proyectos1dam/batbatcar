package boladedrac;
import java.util.Scanner;

public class Funciones {

    static Scanner teclado = new Scanner(System.in);
    final static int MAXIMOS_INTENTOS_PERMITIDOS = 3;
    
//    public static void textoJuego(String cadena) {
//        
//        for (int i = 0; i < cadena.length()-1; i++) {
//            
//            try {
//                System.out.print(cadena.charAt(i));
//                Thread.sleep(100);  
//            }
//            catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//        }
//    }

    //////////////////////////
    // FUNCIONES DE LECTURA //
    //////////////////////////
    public static int leerNumero(String cadena) {
        do {
            System.out.print(cadena);
            if (teclado.hasNextInt()) {
                return teclado.nextInt();
            }
            else {
                datoIncorrecto();
                teclado.next();
            }
        } while (true);
    }
    public static String leerCadena(String cadena) {
        return teclado.next();
    }
    public static void datoIncorrecto() {
        System.out.println("Por favor, introduce un dato correcto");
    }
    
    public static void pulsaCualquierTecla() {
        String seguir;
        System.out.println("¡Escribe cualquier cosa para empezar!");
        try
        {
            seguir = Funciones.leerCadena("");
        }
        catch(Exception e)
        {}
    }

    
    ///////////////////////////////
    // FUNCIONES DE COMPROBACION //
    ///////////////////////////////
    public static boolean comprobarRespuesta(int num) {
        int respuesta;
        
        for (int i = 1; i <= MAXIMOS_INTENTOS_PERMITIDOS; i++) {
            System.out.printf("Intento nº%d: ", i);
            if (teclado.hasNextInt()) {
                respuesta = teclado.nextInt();
                
                if (respuesta == num) {
                    return true;
                }
            }
            else {
                datoIncorrecto();
                teclado.next();
                i--;
            }
        }
        
        return false;
    }
    public static boolean comprobarRespuesta(String cadena) {
        String respuesta;
        
        for (int i = 1; i <= MAXIMOS_INTENTOS_PERMITIDOS; i++) {
            System.out.printf("Intento nº%d: ", i);
            
            respuesta = teclado.next();
            if (respuesta.equals(cadena)) {
                return true;
            }
        }
        
        return false;
    }
    
    
    //////////////////////////////////
    // FUNCIONES PARA OFRECER PISTA //
    //////////////////////////////////
    public static void ofrecerPista(int pista) {
        System.out.println("\nLa pista es: " + pista);
    }
    public static void ofrecerPista(String pista) {
        System.out.println("\nLa pista es: " + pista);
    }
    
    
    ///////////////////////////////////////
    // FUNCIONES DE GENERACION ALEATORIA //
    ///////////////////////////////////////
    public static int generarNumeroAleatorio(int min, int max) {
        return (int)(int) (Math.random() * (max - min + 1)) + min;
    }
    public static String generarCadenaAleatoria(int num) {
        String caracteres = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        String cadena = "";
        int numAleatorio;
        
        for (int i = 0; i < num; i++) {
            numAleatorio = generarNumeroAleatorio(0, caracteres.length()-1);
            cadena += caracteres.charAt(numAleatorio);
        }
        
        return cadena;
    }
    public static String generarCadenaVocalesAleatoria(int num) {
        String caracteres = "aeiou";
        String cadena = "";
        int numAleatorio;
        
        for (int i = 0; i < num; i++) {
            numAleatorio = generarNumeroAleatorio(0, caracteres.length()-1);
            cadena += caracteres.charAt(numAleatorio);
        }
        return cadena;
    }
    
    
    //////////////////////////
    // FUNCIONES DE CALCULO //
    //////////////////////////
    public static int calcularSumatorio(int a, int b) {
        int sumatorio = 0;
        
        for (int i = a; i <= b; i++) {
            sumatorio += i;
        }
        
        return sumatorio;
    }
    public static int calcularFactorial(int a) {
        int sumatorio = 1;
        
        for (int i = a; i > 0; i--) {
            sumatorio *= i;
        }
        
        return sumatorio;
    }
    public static int calcularMinimoComunMultiple(int a, int b, int c, int d) {
        int mcm = 0;
        int i = 1;
        
        while (true) {
            if (i % a == 0 && i % b == 0 && i % c == 0 && i % d == 0) {
                mcm = i;
                break;
            }
            i++;
        }
        
        return mcm;
    }
    
    
    ///////////////////////////////////////
    // FUNCIONES DE GESTION DE STRINGS ////
    ///////////////////////////////////////
    public static String mezclaCadenas(String cad1, String cad2) {
        String mezcla = "";
        
        for (int i = 0, j = cad1.length()-1; i < cad1.length(); i++, j--) {
            mezcla += cad1.charAt(i);
            mezcla += cad2.charAt(j);
        }
        
        return mezcla;
    }
    
    public static int compararCadenaVocales(String cadena, String cadenaVocales) {
        int contador = 0;
        char primeraVocal = cadenaVocales.charAt(0);
        char segundaVocal = cadenaVocales.charAt(1);
        
        for (int i = 0; i < cadena.length()-1; i++) {
            if (cadena.charAt(i) == primeraVocal) {
                if (cadena.charAt(i+1) == segundaVocal) {
                    contador++;
                    i++;
                }
            }
        }
        return contador;
    }
    
    public static int compararCadenaVocalesInversa(String cadena, String cadenaVocales) {
        int contadorInverso = 0;
        char primeraVocal = cadenaVocales.charAt(0);
        char segundaVocal = cadenaVocales.charAt(1);
        
        for (int i = 1; i < cadena.length()-1; i++) {
            if (cadena.charAt(i) == primeraVocal) {
                if (cadena.charAt(i-1) == segundaVocal) {
                    if (cadena.charAt(i+1) != (cadena.charAt(i-1))) {
                        contadorInverso++;
                        i++;
                    }
                }
            }
        }
        return contadorInverso;
    }
    
    public static void limpiarPantalla() {
        System.out.println();System.out.println();System.out.println();System.out.println();System.out.println();
        System.out.println();System.out.println();System.out.println();System.out.println();System.out.println();
        System.out.println();System.out.println();System.out.println();System.out.println();System.out.println();
        System.out.println();System.out.println();System.out.println();System.out.println();System.out.println();
        System.out.println();System.out.println();System.out.println();System.out.println();System.out.println();
    }
    
}
